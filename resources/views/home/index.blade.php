@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 offset-md-2 mt-4">
                <div class="card card-default">
                    <div class="card-header">
                        <h3>URL Shortener</h3>
                    </div>

                    <div class="card-body">
                        <url-shortener></url-shortener>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection