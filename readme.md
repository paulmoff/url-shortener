### Requirements

* Composer installed globally
* NPM

## Installation

Git clone this repository, cd into it and run `./install` or `bash install`

Composer should be installed globally, otherwise you will have to run commands from install script one-by-one manually.

Enter database credentials in `.env` file and run migrations via `php artisan migrate`

## Server

You can use local PHP server: cd into public directory `cd public` and run `php -S localhost:8080`

Navigate to http://localhost:8080 to see application working.