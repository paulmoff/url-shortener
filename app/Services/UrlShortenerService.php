<?php

namespace App\Services;

use App\Link;

class UrlShortenerService
{
    public function generate($url)
    {
        $hash = $this->makeHash($url);

        // check that we don't have that hash already in database
        while (Link::where('hash', $hash)->count()) {
            $hash = $this->makeHash($url);
        }

        // save to database
        $link = new Link;
        $link->link = $url;
        $link->hash = $hash;
        $link->saveOrFail();

        return $hash;
    }

    private function makeHash($url, $padding = 5)
    {
        return substr(hash('sha256', str_random(32) . microtime()), 0, $padding);
    }
}