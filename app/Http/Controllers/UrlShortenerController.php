<?php

namespace App\Http\Controllers;

use App\Http\Requests\UrlRequest;
use App\Link;
use App\Services\UrlShortenerService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\URL;

class UrlShortenerController extends Controller
{
    public function shorten(UrlRequest $request, UrlShortenerService $urlShortenerService)
    {
        $hash = $urlShortenerService->generate($request->get('url'));

        return response()->json([
            'link' => URL::to('/') . '/' . $hash,
        ]);
    }

    public function redirect($hash)
    {
        $link = Link::where('hash', $hash)->firstOrFail();

        return redirect($link->link);
    }
}
