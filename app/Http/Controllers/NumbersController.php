<?php

namespace App\Http\Controllers;

use App\Number;

class NumbersController extends Controller
{
    public function generate()
    {
        try {
            $number = new Number;
            $number->number = random_int(0, PHP_INT_MAX);
            $number->save();

            return $number->number;

        } catch (\Exception $e) {
             abort(500, 'Internal server error');
        }
    }

    public function retrieve(Number $number)
    {
        return $number->number;
    }
}
